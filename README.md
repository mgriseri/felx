# felx

## Manual:

felx: "For each line, execute"

MIT License - Copyright (c) 2020 Michaël Griseri

### DESCRIPTION:

Reads a list of file paths from stdin and executes a given command on each one.

Despite the name, felx is not a for-loop. Commands are executed in parallel.

### USAGE:

    <STDIN> | felx "COMMAND_TEMPLATE"

COMMAND_TEMPLATE is a command containing special variables:

`@path`           The line content (a file path).

`@name`           The name of the file.

`@basename`        The name of the file without extension.

`@parent`         The parent directory path.

Special characters will automatically be escaped.

### EXAMPLES:

Copy all PDF files into one folder, for backup:

    find ~/Documents -iname "*.pdf" | felx "cp @path ~/pdf-backup/@name"

Convert several images or audio files with imagemagick or ffmpeg

    ls *.jpg | felx "convert @path ~/Images/@basename.png"
    ls *.mp3 | felx "ffmpeg -i @path @basename.ogg"

## Build and install (Linux or Mac)

    git clone https://gitlab.com/mgriseri/felx
    cd felx
    nim c -d:release --opt:size --threads:on ./felx.nim
    chmod +x ./felx
    sudo mv ./felx /usr/local/bin/

Uninstall with: `sudo rm /usr/local/bin/felx`
