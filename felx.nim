# Compile with:
# nim c -d:release --opt:size --threads:on ./felx.nim

from os import commandLineParams, extractFilename, dirExists, fileExists, parentDir, splitFile
from osproc import execCmd
from sequtils import filter, map
from strutils import split, replace

const LINE_VAR: string = "@path"
const FILENAME_VAR: string = "@name"
const FILENAME_NO_EXT_VAR: string = "@basename"
const FILENAME_PARENT_VAR: string = "@parent"
const MAX_NB_OF_THREADS: int = 5

if (commandLineParams().len != 1) or (commandLineParams()[0] == "--help"):
  echo "\e[1mfelx\e[0m: \"For each line, execute\""
  echo "MIT License - Copyright (c) 2020 Michaël Griseri"
  echo "\n\e[1mDESCRIPTION:\e[0m"
  echo "Reads a list of file paths from stdin and executes a given command on each one."
  echo "Despite the name, \e[1mfelx\e[0m is not a for-loop. Commands are executed in parallel."
  echo "\n\e[1mUSAGE:\e[0m"
  echo ""
  echo "\t<STDIN> | \e[1mfelx\e[0m \"COMMAND_TEMPLATE\""
  echo ""
  echo "COMMAND_TEMPLATE is a command containing special variables:"
  echo "@path\t\tThe line content (a file path)."
  echo "@name\t\tThe name of the file."
  echo "@basename\tThe name of the file without extension."
  echo "@parent\t\tThe parent directory path."
  echo "\nSpecial characters will automatically be escaped."
  echo "\n\e[1mEXAMPLES:\e[0m"
  echo "- Copy all PDF files into one folder, for backup:"
  echo ""
  echo "\tfind ~/Documents -iname \"*.pdf\" | \e[1mfelx\e[0m \"cp @path ~/pdf-backup/@name\""
  echo ""
  echo "- Convert several images or audio files with imagemagick or ffmpeg"
  echo ""
  echo "\tls *.jpg | \e[1mfelx\e[0m \"convert @path ~/Images/@basename.png\""
  echo "\tls *.mp3 | \e[1mfelx\e[0m \"ffmpeg -i @path @basename.ogg\""
  echo ""
  quit()

proc escapeShChars(s: string): string =
  var outp: string = s

  outp = outp.replace(" ", "\\ ")
  outp = outp.replace("'", "\\'")
  outp = outp.replace("\"", "\\\"")
  outp = outp.replace("`", "\\`")

  outp = outp.replace("!", "\\!")
  outp = outp.replace("?", "\\?")
  outp = outp.replace("&", "\\&")
  outp = outp.replace("|", "\\|")
  outp = outp.replace("$", "\\$")
  outp = outp.replace("#", "\\#")
  outp = outp.replace("*", "\\*")
  outp = outp.replace("^", "\\^")
  outp = outp.replace("=", "\\=")
  outp = outp.replace(":", "\\:")
  outp = outp.replace(";", "\\;")


  outp = outp.replace("(", "\\(")
  outp = outp.replace(")", "\\)")
  outp = outp.replace("[", "\\[")
  outp = outp.replace("]", "\\]")
  outp = outp.replace("{", "\\{")
  outp = outp.replace("}", "\\}")
  outp = outp.replace("<", "\\<")
  outp = outp.replace(">", "\\>")

  return outp

proc runSysCmdThread(param: string) {.thread.} =
  var cmdCode: int
  cmdCode = execCmd(param)

proc runSysCmds(cmds: seq[string]) =
  var threads: array[MAX_NB_OF_THREADS, Thread[string]]

  for i in 0 ..< cmds.len:
    createThread[string](threads[i], runSysCmdThread, cmds[i])

  joinThreads(threads[0 ..< cmds.len])

proc main() =
  let args: seq[string] = commandLineParams()
  let argLast: string = args[args.len - 1]

  var stdinLines: seq[string] = stdin.readAll().split("\n")
  var cmdsToExecute: seq[string]

  stdinLines = stdinLines
    .filter(proc (x: string): bool = x != "")


  for line in stdinLines:
    if not (dirExists(line) or fileExists(line)):
      raise newException(OSError, "\e[31mLine content is not a file path.\e[0m")
    
  stdinLines = stdinLines
    .map(proc (x: string): string = return escapeShChars(x))

  for i in 0 ..< stdinLines.len:
    var newCmd: string = argLast

    newCmd = newCmd.replace(LINE_VAR, stdinLines[i])
    newCmd = newCmd.replace(FILENAME_VAR, extractFilename(stdinLines[i]))
    newCmd = newCmd.replace(FILENAME_NO_EXT_VAR, splitFile(stdinLines[i])[1])
    newCmd = newCmd.replace(FILENAME_PARENT_VAR, parentDir(stdinLines[i]))

    cmdsToExecute.insert(newCmd)

  while cmdsToExecute.len >= MAX_NB_OF_THREADS:
    runSysCmds(cmdsToExecute[0 ..< MAX_NB_OF_THREADS])
    cmdsToExecute = cmdsToExecute[MAX_NB_OF_THREADS ..< cmdsToExecute.len]

  runSysCmds(cmdsToExecute)

main()
